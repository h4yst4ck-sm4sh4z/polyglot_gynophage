#!/bin/bash
src="read.asm"
obj="${src}.o"
bin="${src}.bin"
pl="${src}.payload"
nasm -f elf32 ${src} -o ${obj} \
    && ld -m elf_i386 ${obj} -o ${bin}
#./${bin}
d=`for i in $(objdump -d ${bin}|grep "^ " |cut -f2); do echo -n '\x'$i; done;`
echo -ne "$d" > $pl
#---------------------
echo "[*] $src = \"$d\""
echo "[*] $pl saved"
