; nasm -f elf32 write.asm
; ld -m elf_i386 -o write write.o
; objdump -d write | grep 8048 | grep -v ">:" | cut -f2 | perl -p -e 's/ [ \n]*/\\x/g'
; borja@libcrack.so

BITS 32 

section .text
global _start
_start:

xor eax,eax
xor ebx,ebx
xor ecx,ecx
xor edx,edx
jmp short mycall

shellcode:
    ; write(1,buffer,sizeof(buffer)) NR_ 4
    pop ecx
    mov al,4
    mov bl,1
    mov dl, 14
    int 0x80
    ; exit(0)
    mov al,1
    mov bl,1
    int 0x80

mycall:
    call shellcode
    db "hola compadre",0xa
