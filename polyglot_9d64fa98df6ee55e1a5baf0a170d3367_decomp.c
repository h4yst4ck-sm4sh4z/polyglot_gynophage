#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/resource.h>

int main(int argc, char **argv)
{
    uint32_t eax;
    uint32_t esp;

    uint32_t var_0;
    uint32_t var_4;
    uint32_t var_8;
    uint32_t var_12;
    uint32_t var_20;

    esp = esp & 0xf0;
    esp = esp - (0x1e >> 0x4 << 0x4);
    var_20 = 0x0;
    var_20 = 0xea61;
    esp = esp - 0xc;
    setgid(var_20);

    esp = esp + 0x10;
    esp = esp - 0xc;
    setuid(var_20);

    esp = esp + 0x10;
    var_8 = 0x5;
    var_12 = 0x5;
    esp = esp - 0x8;
    setrlimit(0x0, &var_8);

    esp = esp + 0x10;
    eax = getuid();

    if (eax != var_20) {
            esp = esp - 0xc;
            printf("Failed to drop perms.  Yell at an admin\\n");
            esp = esp + 0x10;
            esp = esp - 0xc;
            exit(0xff);
    }

    esp = esp - 0x8;
    eax = mmap(0x41000000, 0x1000, 0x3, 0x22, 0x0, 0x0);
    esp = esp + 0x20;
    var_4 = eax;
    esp = esp - 0x8;
    eax = mmap(0x42000000, 0x1000, 0x3, 0x22, 0x0, 0x0);
    esp = esp + 0x20;
    var_0 = eax;
    *(int8_t *)var_0 = var_4;

    if (var_4 == 0xff) {
            esp = esp - 0xc;
            printf("Could not map shellcode\\n");
            esp = esp + 0x10;
            esp = esp - 0xc;
            exit(0xff);
    }
    if (var_0 == 0xff) {
            esp = esp - 0xc;
            printf("Could not map stack\\n");
            esp = esp + 0x10;
            esp = esp - 0xc;
            exit(0xff);
    }
    esp = esp - 0x4;
    memset(var_4, 0x0, 0x1000);
    esp = esp + 0x10;
    esp = esp - 0x4;
    memset(var_4, 0x0, 0x1000);
    esp = esp + 0x10;
    esp = esp - 0xc;
    fflush(stdout);
    esp = esp + 0x10;
    fread(var_4, 0x1000, 0x1, stdin);
    esp = esp + 0x10;
    esp = esp - 0xc;
    fflush(stdout);
    esp = esp + 0x10;
    esp = esp - 0xc;
    chdir("/");
    esp = esp + 0x10;
    esp = esp - 0x4;
    eax = mprotect(var_4, 0x1000, 0x5);
    esp = esp + 0x10;
    if (eax == 0xff) {
            printf("Could not remove write permission on shellcode buffer.\\n");
            eax = exit(0xff);
    }
    return 0x0;
}
